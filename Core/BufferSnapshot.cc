/**
 * $Id: BufferSnapshot.cc 36 2008-05-17 13:38:57Z james_neko $
 * $Revision: 36 $
 * $Date: 2008-05-17 23:38:57 +1000 (Sat, 17 May 2008) $ 
 * 
 * Copyright (C) 2008 James Clark
 *
 * This file is part of Mangle.
 *
 * Mangle is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Mangle is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "BufferSnapshot.h"

Core::BufferSnapshot::BufferSnapshot(
		const QString &text_,
		const When &when_,
		const QString &when_description_):
	d_text(text_),
	d_when(when_),
	d_when_description(when_description_)
{
}

