/**
 * $Id: BufferManager.cc 37 2008-06-12 17:37:44Z james_neko $
 * $Revision: 37 $
 * $Date: 2008-06-13 03:37:44 +1000 (Fri, 13 Jun 2008) $ 
 * 
 * Copyright (C) 2008 James Clark
 *
 * This file is part of Mangle.
 *
 * Mangle is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Mangle is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "BufferManager.h"

Core::BufferManager *Core::BufferManager::d_instance = NULL;

Core::BufferManager *
Core::BufferManager::instance()
{
	if (d_instance == NULL)
	{
		d_instance = new BufferManager();
	}
	return d_instance;
}


Core::BufferManager::BufferManager()
{
	// big do nothing
}


Core::BufferManager::buffer_iterator
Core::BufferManager::new_blank_buffer()
{
	// Empty Buffer.
	Buffer *buffer = new Buffer();
	buffer->blank();
	
	// Add and return iterator.
	d_buffer_list.push_back(buffer);
	buffer_iterator bit = d_buffer_list.end();
	return --bit;
}


QList<Core::BufferManager::buffer_iterator>
Core::BufferManager::load_files(
		QStringList filenames)
{
	QList<buffer_iterator> newly_created_buffers;
	
	foreach (QString filename, filenames)
	{
		// Load from disk.
		// FIXME: Try/Catch here!
		Buffer *buffer = new Buffer();
		buffer->load_from_disk(filename);

		// No exceptions? Okay, it's officially loaded.
		d_buffer_list.push_back(buffer);
		buffer_iterator bit = d_buffer_list.end();
		newly_created_buffers.push_back(--bit);
		
		// FIXME: Emit some kind of signal? Or let the caller add the EditWidgets.

//		BufferWidget *buffer_widget = create_new_buffer();
//		buffer_widget->read_file(filename);
//		int tab = w_buffer_tabs->indexOf(buffer_widget);
//		if (tab != -1)
//		{
//			w_buffer_tabs->setTabText(tab, filename);
//		}
	}
	
	return newly_created_buffers;
}



