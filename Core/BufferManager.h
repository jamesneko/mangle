/**
 * $Id: BufferManager.h 37 2008-06-12 17:37:44Z james_neko $
 * $Revision: 37 $
 * $Date: 2008-06-13 03:37:44 +1000 (Fri, 13 Jun 2008) $ 
 * 
 * Copyright (C) 2008 James Clark
 *
 * This file is part of Mangle.
 *
 * Mangle is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Mangle is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef CORE_BUFFERMANAGER_H
#define CORE_BUFFERMANAGER_H

#include <QObject>
#include <list>
#include <QList>
#include <QStringList>

#include "Buffer.h"


namespace Core
{
	/**
	 * That which manages buffers. I mean, come on.
	 * Uses Singleton pattern.
	 */
	class BufferManager :
			public QObject
	{
		Q_OBJECT
		
	public:
		
		typedef std::list<Buffer *> buffer_list_type;
		typedef buffer_list_type::iterator buffer_iterator;
		
		static
		BufferManager *
		instance();
		
		virtual
		~BufferManager()
		{  }
		
		
		/**
		 * Creates a new blank buffer.
		 * This does not directly create a new BufferWidget to edit them with.
		 */
		buffer_iterator
		new_blank_buffer();
		
		/**
		 * Loads new text files from disk into the application.
		 * This does not directly create a new BufferWidget to edit them with.
		 */
		QList<buffer_iterator>
		load_files(
				QStringList filenames);
		

		buffer_iterator
		buffer_list_begin()
		{
			return d_buffer_list.begin();
		}

		buffer_iterator
		buffer_list_end()
		{
			return d_buffer_list.end();
		}
		
	protected:
	
		explicit
		BufferManager();

	private:
		
		buffer_list_type d_buffer_list;

		static BufferManager *d_instance;
	};
}

#endif	//CORE_BUFFERMANAGER_H
	
