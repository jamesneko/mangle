/**
 * $Id: Buffer.h 37 2008-06-12 17:37:44Z james_neko $
 * $Revision: 37 $
 * $Date: 2008-06-13 03:37:44 +1000 (Fri, 13 Jun 2008) $ 
 * 
 * Copyright (C) 2008 James Clark
 *
 * This file is part of Mangle.
 *
 * Mangle is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Mangle is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef CORE_BUFFER_H
#define CORE_BUFFER_H

#include <QObject>

#include "BufferSnapshot.h"


namespace Core
{
	/**
	 * Conceptual interface to a buffer.
	 * Expect iterators and pointers to these to be passed around.
	 * Doesn't necessarily contain the raw text.
	 */
	class Buffer :
			public QObject
	{
		Q_OBJECT
		
	public:
		
		typedef std::list<BufferSnapshot *> snapshot_list_type;
		typedef snapshot_list_type::iterator snapshot_list_iterator;
		typedef snapshot_list_type::const_iterator snapshot_list_const_iterator;

		/**
		 * Construct a new Buffer. Should only be done by BufferManager.
		 */
		explicit
		Buffer();
		
		virtual
		~Buffer();
		
		
		/**
		 * Blanks the buffer.
		 * This needs to be done if you are creating a new, empty buffer for the start of
		 * the editor, or maybe you just wanna murder the current text. I dunno.
		 * 
		 * It pushes a new blank BufferSnapshot.
		 */
		void
		blank();
		
		/**
		 * Loads text from disk.
		 * It gets read into a BufferSnapshot, which gets added to d_snapshots, which we can then...
		 * FIXME: Do what with? Associate with a BufferWidget? Might not be one yet. Or ever!
		 *
		 * This function may throw disk IO related exceptions.
		 */
		void
		load_from_disk(
				const QString &filename);
		
		
		snapshot_list_iterator
		snapshots_begin()
		{
			return d_snapshots.begin();
		}
		
		snapshot_list_iterator
		snapshots_end()
		{
			return d_snapshots.end();
		}
		
	private:
	
		/**
		 * Private, undefined Copy constructor.
		 * Done this way because we cannot copy-construct our base QObject.
		 */
		Buffer(
				const Buffer &other);
		
		/**
		 * A list of BufferSnapshot *s - which contain the raw text, such as when we first loaded
		 * the buffer, just before we did a filter, after we last saved, etc.
		 */
		snapshot_list_type d_snapshots;
	};
}

#endif	//CORE_BUFFER_H

