/**
 * $Id: BufferSnapshot.h 37 2008-06-12 17:37:44Z james_neko $
 * $Revision: 37 $
 * $Date: 2008-06-13 03:37:44 +1000 (Fri, 13 Jun 2008) $ 
 * 
 * Copyright (C) 2008 James Clark
 *
 * This file is part of Mangle.
 *
 * Mangle is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Mangle is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef CORE_BUFFERSNAPSHOT_H
#define CORE_BUFFERSNAPSHOT_H

#include <QString>

namespace Core
{
	/**
	 * Contains raw text of a buffer. For example, one is created when we first
	 * load a file (since we have to put the text -somewhere-), and at other checkpoints
	 * while the file is loaded.
	 */
	class BufferSnapshot
	{

	public:
		
		enum When
		{
			BLANK_SLATE,
			LOADED_FROM_FILE,
		};

		/**
		 * Construct a new BufferSnapshot.
		 * 'when' should be one of the enumerations above,
		 * 'when_description' is a localised string such as "Initial load", "Before filter 'Tabs to Spaces'", "After save", etc.
		 */
		explicit
		BufferSnapshot(
				const QString &text_,
				const When &when_,
				const QString &when_description_);
		
		virtual
		~BufferSnapshot()
		{  }
		
		const QString &
		text() const
		{
			return d_text;
		}
		
		const When
		when() const
		{
			return d_when;
		}

		const QString &
		when_description() const
		{
			return d_when_description;
		}
				
	private:
	
		/**
		 * Private, undefined Copy constructor. We don't want automagical copies of this.
		 */
		BufferSnapshot(
				const BufferSnapshot &other);
		
		/**
		 * The contents of the snapshot. Thank heavens for QString's implicit sharing!
		 */
		QString d_text;
		
		/**
		 * When was this snapshot created?
		 */
		When d_when;
		QString d_when_description;
	};
}

#endif	//CORE_BUFFERSNAPSHOT_H

