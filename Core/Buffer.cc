/**
 * $Id: Buffer.cc 37 2008-06-12 17:37:44Z james_neko $
 * $Revision: 37 $
 * $Date: 2008-06-13 03:37:44 +1000 (Fri, 13 Jun 2008) $ 
 * 
 * Copyright (C) 2008 James Clark
 *
 * This file is part of Mangle.
 *
 * Mangle is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Mangle is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QFile>
#include <QTextStream>

#include "Buffer.h"

Core::Buffer::Buffer()
{
	// Constructor. big do nothing for now.
}

Core::Buffer::~Buffer()
{
	// Destructor. big do nothing for now.
}



void
Core::Buffer::blank()
{
	d_snapshots.push_back(new BufferSnapshot("", BufferSnapshot::BLANK_SLATE, tr("Blank")));
}


void
Core::Buffer::load_from_disk(
		const QString &filename)
{
	QFile file(filename);
	
	bool success = file.open(QFile::ReadOnly);
	// FIXME: Exception if success == false!
	
//	QApplication::setOverrideCursor(Qt::WaitCursor);		// FIXME: maybe this should be put outside this function.
	
	// Grab the text and make a snapshot out of it.
	// FIXME: We could probably do better than instream.readAll().
	QTextStream instream(&file);
	QString text = instream.readAll();
	QString when_description = tr("Loaded from '%1'").arg(filename);
	d_snapshots.push_back(new BufferSnapshot(text, BufferSnapshot::LOADED_FROM_FILE, when_description));
	
//	QApplication::restoreOverrideCursor();
}


