/**
 * $Id: main.cc 37 2008-06-12 17:37:44Z james_neko $
 * $Revision: 37 $
 * $Date: 2008-06-13 03:37:44 +1000 (Fri, 13 Jun 2008) $ 
 * 
 * Copyright (C) 2008 James Clark
 *
 * This file is part of Mangle.
 *
 * Mangle is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Mangle is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QApplication>

#include "Core/BufferManager.h"
#include "Gui/EditorWindow.h"

int main(int argc, char *argv[])
{
	Q_INIT_RESOURCE(mangle);
	QApplication app(argc, argv);

	Core::BufferManager *buffers = Core::BufferManager::instance();

	Gui::EditorWindow window;
	window.show();

	QStringList filenames = app.arguments();
	filenames.pop_front();
	if (filenames.size() > 0)
	{
		buffers->load_files(filenames);
	}
	else
	{
		buffers->new_blank_buffer();
	}
	
	return app.exec();
}

