# QMake project file for Mangle.

TEMPLATE		=	app
TARGET		=	mangle
CONFIG		+=	qt debug

RESOURCES	=	Resources/mangle.qrc
LIBS			+=	-lqscintilla2
RCC_DIR		=	Resources/
MOC_DIR		=	qmake-moc/
UI_DIR		=	qmake-ui/
OBJECTS_DIR	=	qmake-o/


SOURCES		+=	main.cc

HEADERS		+=	Core/BufferManager.h
SOURCES		+=	Core/BufferManager.cc

HEADERS		+=	Core/Buffer.h
SOURCES		+=	Core/Buffer.cc

HEADERS		+=	Core/BufferSnapshot.h
SOURCES		+=	Core/BufferSnapshot.cc

FORMS			+=	Gui/EditorWindow.ui
HEADERS		+=	Gui/EditorWindow.h
SOURCES		+=	Gui/EditorWindow.cc

FORMS			+=	Gui/BufferWidget.ui
HEADERS		+=	Gui/BufferWidget.h
SOURCES		+=	Gui/BufferWidget.cc



