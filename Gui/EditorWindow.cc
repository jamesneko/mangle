/**
 * $Id: EditorWindow.cc 37 2008-06-12 17:37:44Z james_neko $
 * $Revision: 37 $
 * $Date: 2008-06-13 03:37:44 +1000 (Fri, 13 Jun 2008) $ 
 * 
 * Copyright (C) 2008 James Clark
 *
 * This file is part of Mangle.
 *
 * Mangle is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Mangle is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QtGui>
#include <QDebug>
#include <iostream>

#include "EditorWindow.h"


Gui::EditorWindow::EditorWindow()
{
	setupUi(this);
	Core::BufferManager *buffers = Core::BufferManager::instance();
	
	setWindowTitle("Mangle");
	
	w_buffer_tabs->clear();
	Core::BufferManager::buffer_iterator initial_buffer = buffers->new_blank_buffer();
	d_buffer_widget_ptr = create_new_widget_for_buffer(initial_buffer);
	// FIXME: or should we have that appear based on the fact that BM just created something..?
	
	QObject::connect(action_Open_File, SIGNAL(triggered()),
			this, SLOT(open_file()));
	QObject::connect(action_Save_File, SIGNAL(triggered()),
			this, SLOT(save_file()));
	QObject::connect(action_Read_File, SIGNAL(triggered()),
			this, SLOT(read_file()));
	QObject::connect(action_Quit, SIGNAL(triggered()),
			this, SLOT(close()));

	QObject::connect(action_Select_Line, SIGNAL(triggered()),
			this, SLOT(select_line()));
}


void
Gui::EditorWindow::set_tab_title_for_widget(
		Gui::BufferWidget *buffer_widget,
		const QString &title_text)
{
	int tab = get_tab_by_widget(buffer_widget);
	if (tab != -1)
	{
		w_buffer_tabs->setTabText(tab, title_text);
	}
}


void
Gui::EditorWindow::open_file()
{
	Core::BufferManager *buffers = Core::BufferManager::instance();

	QStringList filenames;
	filenames << QFileDialog::getOpenFileName(this);
	if (filenames.size() > 0)
	{
		buffers->load_files(filenames);
	}
}


void
Gui::EditorWindow::save_file()
{
	QString filename = QFileDialog::getSaveFileName(this);
	if ( ! filename.isEmpty())
	{
		get_current_widget()->write_file(filename);
	}
}


void
Gui::EditorWindow::read_file()
{
	QString filename = QFileDialog::getOpenFileName(this);
	if ( ! filename.isEmpty())
	{
		get_current_widget()->read_file(filename);
	}
}


void
Gui::EditorWindow::select_line()
{
	get_current_widget()->select_line();
}


Gui::BufferWidget *
Gui::EditorWindow::create_new_widget_for_buffer(
		Core::BufferManager::buffer_iterator buffer)
{
	Gui::BufferWidget *buffer_widget = new Gui::BufferWidget(this, this, buffer);
	w_buffer_tabs->addTab(buffer_widget, "Newly Opened Buffer");
	
	return buffer_widget;
}


Gui::BufferWidget *
Gui::EditorWindow::get_widget_by_tab(
		int idx)
{
	return static_cast<Gui::BufferWidget *>(w_buffer_tabs->widget(idx));
}

Gui::BufferWidget *
Gui::EditorWindow::get_current_widget()
{
	return get_widget_by_tab(w_buffer_tabs->currentIndex());
}

int
Gui::EditorWindow::get_tab_by_widget(
		Gui::BufferWidget *buffer_widget)
{
	return w_buffer_tabs->indexOf(buffer_widget);
}


