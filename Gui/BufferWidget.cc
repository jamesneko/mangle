/**
 * $Id: BufferWidget.cc 37 2008-06-12 17:37:44Z james_neko $
 * $Revision: 37 $
 * $Date: 2008-06-13 03:37:44 +1000 (Fri, 13 Jun 2008) $ 
 * 
 * Copyright (C) 2008 James Clark
 *
 * This file is part of Mangle.
 *
 * Mangle is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Mangle is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QtGui>
#include <Qsci/qsciscintilla.h>

#include "BufferWidget.h"
#include "EditorWindow.h"



Gui::BufferWidget::BufferWidget(
		QWidget *parent_,
		EditorWindow *editor_window,
		Core::BufferManager::buffer_iterator buffer_):
	QWidget(parent_),
	d_editor_window_ptr(editor_window),
	d_buffer(buffer_)
{
	setupUi(this);
	
	// Set up our internal stuff.
	// Note that at this point, we aren't in a tab yet, so we can't set a title or icon.
	w_scintilla->setUtf8(true);
	QFont font("Monospace");
	w_scintilla->setFont(font);
}


void
Gui::BufferWidget::update_to_latest_snapshot()
{
	Core::Buffer::snapshot_list_iterator begin = (*d_buffer)->snapshots_begin();
	Core::Buffer::snapshot_list_iterator end = (*d_buffer)->snapshots_end();
	if (begin != end)
	{
		// Grab most recent snapshot.
		Core::BufferSnapshot *snapshot_ptr = *(--end);
		w_scintilla->setText(snapshot_ptr->text());
		
		// We get to set our OWN titles!
		d_editor_window_ptr->set_tab_title_for_widget(this, snapshot_ptr->when_description());
	}
	
}


void
Gui::BufferWidget::read_file(
		const QString &filename)
{
	// FIXME: REMOVE
	QFile file(filename);
	
	if ( ! file.open(QFile::ReadOnly))
	{
		QMessageBox::warning(this, tr("Read Error"),
											tr("Error opening file '%1' for reading:\n%2.")
											.arg(filename)
											.arg(file.errorString())
											);
		return;
	}
	
	QTextStream instream(&file);
	QApplication::setOverrideCursor(Qt::WaitCursor);
	w_scintilla->setText(instream.readAll());
	QApplication::restoreOverrideCursor();
	
	//editor window-> //statusBar()->showMessage(tr("File loaded"), 2000);
}


void
Gui::BufferWidget::write_file(
		const QString &filename)
{
	// FIXME: Delegate actual I/O to the Buffer!
	QFile file(filename);
	
	if ( ! file.open(QFile::WriteOnly))
	{
		QMessageBox::warning(this, tr("Write Error"),
											tr("Error opening file '%1' for writing:\n%2.")
											.arg(filename)
											.arg(file.errorString())
											);
		return;
	}
	
	QTextStream outstream(&file);
	QApplication::setOverrideCursor(Qt::WaitCursor);
	outstream << w_scintilla->text();
	QApplication::restoreOverrideCursor();
}

void
Gui::BufferWidget::select_line()
{
	int line;
	int index;
	w_scintilla->getCursorPosition(&line, &index);
	int length = w_scintilla->lineLength(line);
	
	w_scintilla->setSelection(line, 0, line, length);
}

