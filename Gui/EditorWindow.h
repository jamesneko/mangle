/**
 * $Id: EditorWindow.h 37 2008-06-12 17:37:44Z james_neko $
 * $Revision: 37 $
 * $Date: 2008-06-13 03:37:44 +1000 (Fri, 13 Jun 2008) $ 
 * 
 * Copyright (C) 2008 James Clark
 *
 * This file is part of Mangle.
 *
 * Mangle is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Mangle is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef GUI_EDITORWINDOW_H
#define GUI_EDITORWINDOW_H

#include <QMainWindow>
#include <QStringList>
#include <QDockWidget>

#include "ui_EditorWindow.h"

#include "BufferWidget.h"


namespace Gui
{
	/**
	 * Handles the main user interface. Signals, slots, buttons, widgets,
	 * That sort of thing. No text manipulation or text edit widget stuff should go here.
	 */
	class EditorWindow :
			public QMainWindow,
			protected Ui_EditorWindow
	{
		Q_OBJECT
		
	public:
		explicit
		EditorWindow();
	

		/**
		 * Used by BufferWidgets to set useful information in their tab title.
		 */
		void
		set_tab_title_for_widget(
				Gui::BufferWidget *buffer_widget,
				const QString &title_text);

	public slots:
		
		void
		open_file();

		void
		save_file();

		void
		read_file();
		
		void
		select_line();
		
	private:
	
		Gui::BufferWidget *
		create_new_widget_for_buffer(
				Core::BufferManager::buffer_iterator buffer);

		Gui::BufferWidget *
		get_widget_by_tab(
				int idx);

		Gui::BufferWidget *
		get_current_widget();

		int
		get_tab_by_widget(
				Gui::BufferWidget *buffer_widget);
		

		BufferWidget *d_buffer_widget_ptr;
	
	};
}



#endif // GUI_EDITORWINDOW_H
