/**
 * $Id: BufferWidget.h 37 2008-06-12 17:37:44Z james_neko $
 * $Revision: 37 $
 * $Date: 2008-06-13 03:37:44 +1000 (Fri, 13 Jun 2008) $ 
 * 
 * Copyright (C) 2008 James Clark
 *
 * This file is part of Mangle.
 *
 * Mangle is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Mangle is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef GUI_BUFFERWIDGET_H
#define GUI_BUFFERWIDGET_H

#include <QWidget>
#include <QDockWidget>

#include "ui_BufferWidget.h"

#include "Core/BufferManager.h"

namespace Gui
{
	class EditorWindow;

	/**
	 * A BufferWidget is one view of a Buffer.
	 *
	 * FIXME: This should ultimately be an ABC. We could have multiple types of BufferWidget,
	 * each feeding off a Buffer.
	 */
	class BufferWidget :
			public QWidget,
			protected Ui_BufferWidget
	{
		Q_OBJECT
		
	public:

		explicit
		BufferWidget(
				QWidget *parent_,
				EditorWindow *editor_window,
				Core::BufferManager::buffer_iterator buffer_);
	
		void
		update_to_latest_snapshot();
		
		void
		read_file(
				const QString &filename);

		void
		write_file(
				const QString &filename);
	
		void
		select_line();
		
	private:
	
		EditorWindow *d_editor_window_ptr;
		
		Core::BufferManager::buffer_iterator d_buffer;
	};
}



#endif // GUI_BUFFERWIDGET_H
